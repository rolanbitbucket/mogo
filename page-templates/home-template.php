<?php
/*
Template Name: Page: Home Template
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package rs-theme
*/

get_header();

// Get site settings 
$options = get_option( 'rs_theme_theme_options' );
?>


 <div id="wrapper">
    <!-- Slideshow 2 -->
    <div class="rslides_container">
      <ul class="rslides" id="slider2">
        <li><img src="<?php the_field('slide-image_1'); ?>" /></li>
        <li><img src="<?php the_field('slide-image_2'); ?>" /></li>
        <li><img src="<?php the_field('slide-image_3'); ?>" /></li>
      </ul>
      <div class="overlay"></div>
    </div>
  </div>
<?php

get_footer();
