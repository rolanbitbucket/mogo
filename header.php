<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rs-theme
 */

$options = get_option( 'rs_theme_theme_options' );
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">



<?php
wp_head();

// display tracking or analytics code
if( isset( $options['header_analytics'] ) ) {
	echo $options['header_analytics'];
}
?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'rs-theme' ); ?></a>
	
	<?php
	$masthead_style = '';
	if( get_header_image() ) {
		$masthead_style = 'background-image: url('. get_header_image() .') no-repeat 0 0; background-size: cover';
	}
	?>
	<header id="masthead" class="site-header" role="banner" style="<?php echo $masthead_style; ?>">
		<div class="container">
			<div class="row align-items-center nav-header">
				<div class="col-lg-4 col-padding-0">

					<div class="site-branding">
						<?php
						if ( is_front_page() && is_home() ) { ?>
							<h1 class="site-title mb-0">

								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">

								<?php  
								if( empty( $options['logo']) ){ ?>
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
										<span class="header-logo"><?php echo $options['text_logo']; ?></span>
										</a>
									<?php } else { ?>
									<img class="d-block" src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php bloginfo('name') ?>" />
								<?php  }   ?>
							


								</a>

							</h1>
						<?php } else { ?>
							<p class="site-title mb-0">

									<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">

								<?php  
								if( empty( $options['logo']) ){ ?>
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
										<span class="header-logo"><?php echo $options['text_logo']; ?></span>
										</a>
									<?php } else { ?>
									<img class="d-block" src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php bloginfo('name') ?>" />
								<?php  }   ?>
							


								</a>


								<!-- <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">

									<?php if( isset( $options['logo'] ) ) { ?>

										<img src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php bloginfo('name') ?>" />
										

									<?php } else { ?>

										<?php bloginfo( 'name' ); ?>

									<?php } ?>
								</a> -->
							</p>
						<?php
						}

						$description = get_bloginfo( 'description', 'display' );
						if ( $description || is_customize_preview() ) : ?>
							<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
						<?php
						endif; ?>
					</div><!-- .site-branding -->
					
				</div>
				
				<div class="col-lg-8 text-right col-padding-0">
					
					<nav id="site-navigation" class="main-navigation" role="navigation">
						
						<div class="container">
							<div class="row align-items-center">
								
								<div class="primary-menu-wrapper">
									<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
										<i class="fa fa-bars" aria-hidden="true"></i>
									</button>
									
									<?php wp_nav_menu( array( 
										'theme_location' => 'primary',
										'menu_id' 		 => 'primary-menu',
										'menu_class' 	 => 'primary-menu',
										'container' 	 => 'ul'
									) ); ?>
									<ul class="secondary-widget-menu">
										<li>
											<?php if ( is_active_sidebar( 'cart-widget' ) ) : ?>
		                                    <div id="secondary" class="widget-area" role="complementary">
		                                        <?php dynamic_sidebar( 'cart-widget' ); ?>
		                                    </div>
                                        	<?php endif; ?>
										</li>
										<li>
											<?php if ( is_active_sidebar( 'search-widget' ) ) : ?>
		                                    <div id="secondary" class="widget-area" role="complementary">
		                                        <?php dynamic_sidebar( 'search-widget' ); ?>
		                                    </div>
		                                    <?php endif; ?>
										</li>
									</ul>
								</div>

									
							
							</div>
						</div>

					</nav><!-- #site-navigation -->
				</div>
			</div>
		</div>
	</header><!-- #masthead -->


	<div id="content" class="site-content">
