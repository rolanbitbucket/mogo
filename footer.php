<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rs-theme
 */

$options = get_option( 'rs_theme_theme_options' );
?>
</div><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10 text-center">
				<div class="footer-contents"> 
					<div class="row">
						<div class="col-md-4">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
								<?php  
								if( empty( $options['logo']) ){ ?>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
										<span class="header-logo"><?php echo $options['text_logo']; ?></span>
									</a>
								<?php } else { ?>
									<img class="d-block" src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php bloginfo('name') ?>" />
								<?php  }   ?>
							</a>
						<small class="text-muted">
							<p class="site-description">Welcome to WordPress. This is your first post. Edit or delete it,</p>
						</small>
							<strong>
								<h3>15k</h3> <p>followers</p>
							</strong>
							<hr/>
							<div class="row">
								<div class="col-md-12">
									<div class="social-icon">
										<p>Follow Us:</p>
										<ul>
											<li>
												<i class="fab fa-facebook-f"></i>
											</li>
											<li>
												<i class="fab fa-twitter"></i>
											</li>
											<li>
												<i class="fab fa-instagram"></i>
											</li>
											<li>
												<i class="fab fa-pinterest-p"></i>
											</li>
											<li>
												<i class="fab fa-google-plus-g"></i>
											</li>
											<li>
												<i class="fab fa-youtube"></i>
											</li>
											<li>
												<i class="fab fa-dribbble"></i>	
											</li>
											<li>
												<i class="fab fa-tumblr"></i>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-md-12">
									<?php echo do_shortcode('[mc4wp_form id="265"]'); ?>
								</div>
							</div>
							
						</div>
						<div class="col-md-4 p-1">
                                    <h5><b>BLOGS</b></h5>


                                      		<?php  	// the query
                                      		$wpb_all_query = new WP_Query(array('post_type'=>'post','order' => 'ASC', 'post_status'=>'publish', 'posts_per_page'=>4));
                                      		?>

                                      		<?php if ( $wpb_all_query->have_posts()) :  ?>

                                      			<!-- the loop -->     

                                      			<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); 
                                      				?> 
                                      				<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

                                      				<div class="footer-blog"> 
                                      					<div class="row no-gutters">
                                      						<div class="col-md-5 p-0">
                                      							<figure ><?php  the_post_thumbnail( 'full' ); ?> </figure>
                                      						</div> 
                                      						<div class="col-md-7 p-0">
                                      							<div class="footer-text-contents">
                                      								<small>
                                      									<h5><b> <?php the_title(); ?> </b></h5>
                                      								<p><?php the_time('M j, Y') ?></p>
                                      								</small>
                                      												
                                      							</div>
                                      						</div>
                                      					</div> 
                                      				</div>



                                      			<?php endwhile; ?>

                                      			<!-- end of the loop -->

                                      			<?php wp_reset_postdata(); ?>

                                      			<?php else : ?>


                                      				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                                      			<?php endif; ?>   
                                      	</div>

                                      		<div class="col-md-4">

                                      			<h5><b>INSTAGRAM</b></h5>
            <div class="row no-gutters">

             <?php 

			$your_token = '12064161509.1677ed0.bc4dbfe45962470791db4c63aecc1a77'; // read above how to get it

			// I recommend to use "self" if your application is not approved
			$ig_user_id = 'self';

			$remote_wp = wp_remote_get( "https://api.instagram.com/v1/users/" . $ig_user_id . "/media/recent/?access_token=" . $your_token );

			$instagram_response = json_decode( $remote_wp['body'] );

			if( $remote_wp['response']['code'] == 200 ) {

				foreach( $instagram_response->data as $m ) {

					echo '<div class="col-6 col-md-4 p-0">
					<a href="' . $m->link . '" id="media-' . $m->id . '" class="type-' . $m->type . '">
					<img src="' . $m->images->standard_resolution->url . '
					" title="' . $m->caption->text . '
					" width="' . $m->images->standard_resolution->width . '
					" height="' . $m->images->standard_resolution->height . '" />
					</a> 
					</div>';
					// more parameters here https://www.instagram.com/developer/endpoints/users/#get_users_media_recent

				}

			} elseif ( $remote_wp['response']['code'] == 400 ) {
				echo '<b>' . $remote_wp['response']['message'] . ': </b>' . $instagram_response->meta->error_message;
			}?>

		</div>

	</div>
</div>
</div>

<hr/>
<?php
if( isset( $options['footer_text'] ) ) {
	echo $options['footer_text'];
}
?>
</div>
</div>
</div>

</footer><!-- #colophon -->
</div><!-- #page -->

<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900" rel="stylesheet"> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<?php
	// include WP footer for hook and styles
wp_footer();

	// display tracking or analytics code
if( isset( $options['analytics'] ) ) {
	echo $options['analytics'];
}
?>

</body>
</html>



