<?php 
/**
 * @desc	If you have something to add in add_action function add it here.
 * @author	Ryan Sutana
 * @uri		http://www.sutanaryan.com/
 *
 * @package rs-theme
 */

/*
 * Remove default WP builtin actions
 */
remove_action( 'wp_head', 'wp_generator' ); 

/*
 * @desc Localize wp-ajax
 */
function rs_theme_init() {
	wp_enqueue_script( 'rsclean-request-script', get_template_directory_uri() . '/js/ajax.js', array( 'jquery' ) );
	wp_localize_script( 'rsclean-request-script', 'theme_ajax', array(
		'url'		=> admin_url( 'admin-ajax.php' ),
		'site_url' 	=> get_bloginfo('url'),
		'theme_url' => get_bloginfo('template_directory')
	) );
}
add_action( 'init', 'rs_theme_init' );


/**********************************
 * Help site speed
 **********************************/
if ( ! is_admin() ) {
  add_action("wp_enqueue_scripts", "ap_jquery_enqueue", 10 );
}

function ap_jquery_enqueue() {
	wp_deregister_script('jquery');
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '1.12.4', true );
}

// Remove wp version param from any enqueued scripts
function rs_them_remove_wp_ver( $src ) {
   	if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
    	$src = remove_query_arg( 'ver', $src );
   	return $src;
}
add_filter( 'style_loader_src', 'rs_them_remove_wp_ver', 9999 );
add_filter( 'script_loader_src', 'rs_them_remove_wp_ver', 9999 );

function occ_custom_footer() {
	ob_start();
	?>
		<script type="text/javascript">
			function downloadJSAtOnload() {
				var element = document.createElement("script");
				element.src = "<?php echo get_stylesheet_directory_uri(); ?>/js/defer.js";
				document.body.appendChild(element);
			}
			
			if (window.addEventListener)
				window.addEventListener("load", downloadJSAtOnload, false);
			else if (window.attachEvent)
				window.attachEvent("onload", downloadJSAtOnload);
			else window.onload = downloadJSAtOnload;
		</script>
	<?php
	echo ob_get_clean();
}
add_action( 'wp_footer', 'occ_custom_footer' );

/**********************************
 * Customize WP Login
 **********************************/
function rs_theme_login_logo() 
{
	$options = get_option( 'rs_theme_theme_options' );
	?>
	<style type="text/css">
		.login h1 a {
			background: url('<?php echo esc_url( $options['logo'] ) ?>') top center no-repeat;
		}
	</style>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,500" rel="stylesheet">
	<?php 
}
add_action( 'login_head', 'rs_theme_login_logo' );

function rs_login_stylesheet() {
    wp_enqueue_style( 'rstheme-custom-login-css', get_stylesheet_directory_uri() . '/css/login-form.css' );
    wp_enqueue_script( 'rstheme-custom-login-js', get_stylesheet_directory_uri() . '/js/login-form.js' );
}
add_action( 'login_enqueue_scripts', 'rs_login_stylesheet' );