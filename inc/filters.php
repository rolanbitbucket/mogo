<?php
/**
 * @desc	If you have something to add in add_filter function add it here.
 * @author	Ryan Sutana
 * @uri		http://www.sutanaryan.com/
 *
 * @package rs-theme
 */

/*
 * Enable shortcode in Menu and Sidebar
 */
add_filter( 'wp_nav_menu', 'do_shortcode' );
add_filter( 'widget_text', 'do_shortcode' );
add_filter( 'show_admin_bar', '__return_false' );

/*
 * Replace WP Login header URL
 */
function rs_theme_headerurl( $url ) {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'rs_theme_headerurl' );


/**
 * Add data-rel link attribute to attachment media or link
 */
function rs_theme_add_rel_attribute( $link ) {
	return str_replace( '<a href', '<a data-rel="fancybox" href', $link );
}
add_filter( 'wp_get_attachment_link', 'rs_theme_add_rel_attribute' );


/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function rs_theme_custom_excerpt_length( $length ) {
    return 12;
}
add_filter( 'excerpt_length', 'rs_theme_custom_excerpt_length', 9999 );


/**
 * Filter the "read more" on auto excerpt string link to the post.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function rs_theme_excerpt_more( $more ) {
    // return sprintf( '<p><a class="read-more" href="%1$s">%2$s</a></p>',
    //     get_permalink( get_the_ID() ),
    //     __( 'Read More', 'rs-theme' )
    // );
    return '';
}
add_filter( 'excerpt_more', 'rs_theme_excerpt_more' );

/*
 * Removed Continue reading on manual generated excerpt;
 */
function rs_theme_custom_excerpt_more( $output ) {
	global $post;

	if ( has_excerpt() ) {
		$output .= '<p><a class="read-more" href="'. get_permalink( $post->ID ) .'">Read More</a></p>';
	}

	return preg_replace( '/<a[^>]+>Continue reading.*?<\/a>/i', '', $output );
}
add_filter( 'get_the_excerpt', 'rs_theme_custom_excerpt_more', 20 );


/*
 * Remove more-link jumper
 */
function rsthem_remove_more_jump_link( $link ) 
{
	$offset = strpos( $link, '#more-' );
	
	if( $offset )
		$end = strpos( $link, '"', $offset );
	
	if( $end )
		$link = substr_replace( $link, '', $offset, $end-$offset );
	
	return $link;
}
add_filter( 'the_content_more_link', 'rsthem_remove_more_jump_link' );


/*
 * Add new defaults in comment form
 */
function rsthem_new_comment_form_default( $default ) {
	$defaults = array(
		'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.' ) .'</p>',
		'comment_notes_after'  => '<p class="form-allowed-tags">' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s' ), ' <code>' . allowed_tags() . '</code>' ) . '</p>',
	);
	
	return $defaults;
}
add_filter( 'comment_form_defaults', 'rsthem_new_comment_form_default' );

/**
 * Help site speed
 **/

/*
Remove query strings from CSS and JS inclusions
*/
function _remove_script_version($src) {
	$parts = explode('?ver', $src);
	return $parts[0];
}
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );

/*
Force scripts to defer for better performance
*/

/*
Remove jquery migrate for enhanced performance
*/
function remove_jquery_migrate($scripts) {
   	if (is_admin())
		return;

	$scripts->remove('jquery');
	$scripts->add('jquery', false, array('jquery-core'), '1.10.2');
}
add_action( 'wp_default_scripts', 'remove_jquery_migrate' );


/**
 * Allow SVG file-type
 * @param  $mimes file-type to add
 * @return New file-type added
 */
function dreamarchers_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'dreamarchers_mime_types' );




