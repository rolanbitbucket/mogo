<ol class="breadcrumb ml-0">
	<?php
		if(function_exists('bcn_display')) {
			bcn_display();
		} 
		else {
			?>
				<li class="breadcrumb-item"><a href="<?php bloginfo('url'); ?>"><?php _e('Home', 'rs-theme') ?></a></li>
				
				<?php if( is_tag() ) { ?>
					<li class="breadcrumb-item active"><?php _e('Posts Tagged &quot; <strong>', 'rs-theme') . single_tag_title() . _e('</strong> &quot;'); ?></li>
				<?php } elseif (is_day()) { ?>
					<li class="breadcrumb-item active"><?php _e('Posts made in', 'rs-theme') ?> <?php the_time('F jS, Y'); ?></li>
				<?php } elseif (is_month()) { ?>
					<li class="breadcrumb-item active"><?php _e('Posts made in', 'rs-theme') ?> <?php the_time('F, Y'); ?></li>
				<?php } elseif (is_year()) { ?>
					<li class="breadcrumb-item active"><?php _e('Posts made in', 'rs-theme') ?> <?php the_time('Y'); ?></li>
				<?php } elseif (is_search()) { ?>
					<li class="breadcrumb-item active"><?php _e('Search results for', 'rs-theme') ?> <?php the_search_query() ?></li>
				<?php } elseif (is_single()) { ?>
					<?php $category = get_the_category();
						  if (!empty($category)) { 
								$catlink = get_category_link( $category[0]->cat_ID );
								echo '<li class="breadcrumb-item"><a href="'.$catlink.'">'.$category[0]->cat_name.'</a></li> 
									<li class="current active">'.get_the_title().'</li>';
						  }; ?>
				<?php } elseif (is_category()) { ?>
					<li class="breadcrumb-item active"><?php single_cat_title(); ?></li>
				<?php } elseif (is_author()) { ?>
					<?php global $wp_query;
						  $curauth = $wp_query->get_queried_object(); ?>
					<li class="breadcrumb-item active"><?php _e('Posts by ', 'rs-theme'); echo ' ', $curauth->nickname; ?></li>
				<?php } elseif (is_page()) { ?>
					<li class="breadcrumb-item active"><?php wp_title(''); ?></li>
				<?php }; ?>
			<?php
		}
	?>
</ol>
<!-- end #breadcrumbs -->