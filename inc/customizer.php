<?php
/**
 * rs-theme Theme Customizer.
 *
 * @package rs-theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function rs_theme_customize_register( $wp_customize ) {
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
    $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage'; 
    // General
    $wp_customize->add_section( 'rs_theme_general_section' , array(
        'title'        => __('General Settings', 'rs_theme'),
        'priority'     => 10,
    ) );
    $wp_customize->add_setting( 'rs_theme_theme_options[logo]', array(
        'default'       => '',
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control( new WP_Customize_Upload_Control( $wp_customize, 'logo', array(
        'label'         => __('Upload Logo', 'rs_theme'),
        'section'       => 'rs_theme_general_section',
        'settings'      => 'rs_theme_theme_options[logo]',
    )));
    $wp_customize->add_setting( 'rs_theme_theme_options[text_logo]', array(
        'default'       => 'MoGo',
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control( 'text_logo', array(
        'label'         => __( 'Text Logo', 'rs_theme' ),
        'description'   => __( '', 'rs_theme' ),
        'section'       => 'rs_theme_general_section',
        'settings'      => 'rs_theme_theme_options[text_logo]',
    ));
    $wp_customize->add_setting( 'rs_theme_theme_options[phone_number]', array(
        'default'       => '',
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control( 'phone_number', array(
        'label'         => __( 'Phone Number', 'rs_theme' ),
        'description'   => __( '', 'rs_theme' ),
        'section'       => 'rs_theme_general_section',
        'settings'      => 'rs_theme_theme_options[phone_number]',
    ));

    $wp_customize->add_setting( 'rs_theme_theme_options[analytics]', array(
        'default'       => '',
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control( 'analytics', array(
        'label'         => __( 'Analytics', 'rs_theme' ),
        'description'   => __( 'Paste your Google Analytics (or other) tracking code here.', 'rs_theme' ),
        'section'       => 'rs_theme_general_section',
        'settings'      => 'rs_theme_theme_options[analytics]',
        'type'          => 'textarea',
    ));

    $wp_customize->add_setting( 'rs_theme_theme_options[header_analytics]', array(
        'default'       => '',
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control( 'header_analytics', array(
        'label'         => __( 'Header Analytics', 'rs_theme' ),
        'description'   => __( 'NOTE: Use this box for analytics that require to be added early or in header area of your site pages, paste your Google Analytics (or other) tracking code here.', 'rs_theme' ),
        'section'       => 'rs_theme_general_section',
        'settings'      => 'rs_theme_theme_options[header_analytics]',
        'type'          => 'textarea',
    ));
    // Blog
    $wp_customize->add_section( 'rs_theme_blog_section' , array(
        'title'        => __('Blog', 'rs_theme'),
        'priority'     => 10,
    ) );

    $wp_customize->add_setting( 'rs_theme_theme_options[enable_post_comment]', array(
        'default'       => 'yes',
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control( 'rs_theme_enable_post_comment', array(
        'label'         => __( 'Enable Comment in Posts', 'rs_theme' ),
        'description'   => __( 'If check it will display comment form in posts.', 'rs_theme' ),
        'section'       => 'rs_theme_blog_section',
        'settings'      => 'rs_theme_theme_options[enable_post_comment]',
        'type'           => 'radio',
        'choices'        => array(
            'yes'   => __( 'Enable' ),
            'no'    => __( 'Disable' )
        )
    ));
    $wp_customize->add_setting( 'rs_theme_theme_options[enable_page_comment]', array(
        'default'       => 'yes',
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control( 'rs_theme_enable_page_comment', array(
        'label'         => __( 'Enable Comment in Pages', 'rs_theme' ),
        'description'   => __( 'If check it will display comment form in pages.', 'rs_theme' ),
        'section'       => 'rs_theme_blog_section',
        'settings'      => 'rs_theme_theme_options[enable_page_comment]',
        'type'           => 'radio',
        'choices'        => array(
            'yes'   => __( 'Enable' ),
            'no'    => __( 'Disable' )
        )
    ));
    // Social Profile Section
    /* to be follow or use widget only */
    // Footer Section
    $wp_customize->add_section( 'rs_theme_footer_section' , array(
        'title'        => __('Footer', 'rs_theme'),
        'priority'     => 200,
    ) );

    $wp_customize->add_setting( 'rs_theme_theme_options[footer_text]', array(
        'default'       => 'Copyright &copy; 2016.',
        'capability'    => 'edit_theme_options',
        'type'          => 'option',
    ));
    $wp_customize->add_control( 'rs_theme_footer_text', array(
        'label'         => __( 'Footer Text', 'rs_theme' ),
        'description'   => __( 'Enter footer text ex. copyright description', 'rs_theme' ),
        'section'       => 'rs_theme_footer_section',
        'settings'      => 'rs_theme_theme_options[footer_text]',
        'type'          => 'textarea',
    ));
    // New section goes here 
}
add_action( 'customize_register', 'rs_theme_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function rs_theme_customize_preview_js() {
    wp_enqueue_script( 'rs_theme_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'rs_theme_customize_preview_js' );







//Set the Post Custom Field in the WP dashboard as Name/Value pair 
function bac_PostViews($post_ID) {
 
    //Set the name of the Posts Custom Field.
    $count_key = 'post_views_count'; 
     
    //Returns values of the custom field with the specified key from the specified post.
    $count = get_post_meta($post_ID, $count_key, true);
     
    //If the the Post Custom Field value is empty. 
    if($count == ''){
        $count = 0; // set the counter to zero.
         
        //Delete all custom fields with the specified key from the specified post. 
        delete_post_meta($post_ID, $count_key);
         
        //Add a custom (meta) field (Name/value)to the specified post.
        add_post_meta($post_ID, $count_key, '0');
        return $count . '';
     
    //If the the Post Custom Field value is NOT empty.
    }else{
        $count++; //increment the counter by 1.
        //Update the value of an existing meta key (custom field) for the specified post.
        update_post_meta($post_ID, $count_key, $count);
         
        //If statement, is just to have the singular form 'View' for the value '1'
        if($count == '1'){
        return $count . '';
        }
        //In all other cases return (count) Views
        else {
        return $count . ' ';
        }
    }
}



//Gets the  number of Post Views to be used later.
function get_PostViews($post_ID){
    $count_key = 'post_views_count';
    //Returns values of the custom field with the specified key from the specified post.
    $count = get_post_meta($post_ID, $count_key, true);
 
    return $count;
}
 
//Function that Adds a 'Views' Column to your Posts tab in WordPress Dashboard.
function post_column_views($newcolumn){
    //Retrieves the translated string, if translation exists, and assign it to the 'default' array.
    $newcolumn['post_views'] = __('Views');
    return $newcolumn;
}
 
//Function that Populates the 'Views' Column with the number of views count.
function post_custom_column_views($column_name, $id){
     
    if($column_name === 'post_views'){
        // Display the Post View Count of the current post.
        // get_the_ID() - Returns the numeric ID of the current post.
        echo get_PostViews(get_the_ID());
    }
}
//Hooks a function to a specific filter action.
//applied to the list of columns to print on the manage posts screen.
add_filter('manage_posts_columns', 'post_column_views');
 
//Hooks a function to a specific action. 
//allows you to add custom columns to the list post/custom post type pages.
//'10' default: specify the function's priority.
//and '2' is the number of the functions' arguments.
add_action('manage_posts_custom_column', 'post_custom_column_views',10,2);





