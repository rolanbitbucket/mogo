<?php 
/**
 * @desc	If you have something to add in add_action function add it here.
 * @author	Ryan Sutana
 * @uri		http://www.sutanaryan.com/
 *
 * @package rs-theme
 */


// register postype works
add_action( 'init', 'custom_post_type_func' );
function custom_post_type_func() {
    //posttypename = services
$labels = array(
'name' => _x( 'Works', 'works' ),
'singular_name' => _x( 'Work', 'work' ),
'add_new' => _x( 'Add New', 'work' ),
'add_new_item' => _x( 'Add New work', 'work' ),
'edit_item' => _x( 'Edit work', 'work' ),
'new_item' => _x( 'New work', 'work' ),
'view_item' => _x( 'View work', 'work' ),
'search_items' => _x( 'Search work', 'work' ),
'not_found' => _x( 'No work found', 'work' ),
'not_found_in_trash' => _x( 'No work found in Trash', 'work' ),
'parent_item_colon' => _x( 'Parent work:', 'work' ),
'menu_name' => _x( 'Work', 'work' ),
);
$args = array(
'labels' => $labels,
'hierarchical' => true,
'description' => 'Hi, this is my custom post type work.',
'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes' ),
'taxonomies' => array( 'category', 'post_tag', 'page-category' ),
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'show_in_nav_menus' => true,
'publicly_queryable' => true,
'exclude_from_search' => false,
'has_archive' => true,
'query_var' => true,
'can_export' => true,
'rewrite' => true,
'show_in_rest' => true,
'capability_type' => 'post',
'menu_icon' => 'dashicons-admin-customizer'
);
register_post_type( 'work', $args );

}
