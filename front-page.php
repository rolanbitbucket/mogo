<?php
/**
* The template for displaying all pages.
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site may use a
* different template.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package rs-theme
*/

get_header();

// Get site settings 
$options = get_option( 'rs_theme_theme_options' );
?>


<div id="slider-wrapper">
	<!-- Slideshow-->
	<div class="rslides_container">
		<ul class="rslides" id="slider2">
			<li>
				<img src="<?php the_field('slide-image_1'); ?>" />
				<div class="overlay"></div>
				<div class="slider-text">
					<h2 class="slide1-heading"><?php the_field('slider-heading'); ?></h2>
					<h1 class="slide1-description"><?php the_field('slider-description'); ?></h1>
					<hr/>
					<button><a href="#"><?php the_field('slider-button-text'); ?></a></button>
				</div>
			</li>
			<li>
				<img src="<?php the_field('slide-image_2'); ?>" />
				<div class="overlay"></div>
				<div class="slider-text">
					<h2 class="slide1-heading"><?php the_field('slider-heading'); ?></h2>
					<h1 class="slide1-description"><?php the_field('slider-description'); ?></h1>
					<hr/>
					<button><a href="#"><?php the_field('slider-button-text'); ?></a></button>
				</div>
			</li>
			<li>
				<img src="<?php the_field('slide-image_3'); ?>" />
				<div class="overlay"></div>
				<div class="slider-text">
					<h2 class="slide1-heading"><?php the_field('slider-heading'); ?></h2>
					<h1 class="slide1-description"><?php the_field('slider-description'); ?></h1>
					<hr/>
					<button><a href="#"><?php the_field('slider-button-text'); ?></a></button>
				</div>
			</li>
			<li>
				<img src="<?php the_field('slide-image_3'); ?>" />
				<div class="overlay"></div>
				<div class="slider-text">
					<h2 class="slide1-heading"><?php the_field('slider-heading'); ?></h2>
					<h1 class="slide1-description"><?php the_field('slider-description'); ?></h1>
					<hr/>
					<button><a href="#"><?php the_field('slider-button-text'); ?></a></button>
				</div>
			</li>
		</ul>
	</div> 
</div>

<main id="main" class="site-main" role="main">

	<!--  about us section -->
	<div class="container" id="about-us-section">
		<div class="row">
			<div class="col-md-12">
				<header class="header">
					<h5 class="header__caption"><?php the_field('what_we_do'); ?></h5>
					<h2 class="header__heading"><?php the_field('about_us_heading') ?></h2>
					<p class="header__description"><?php the_field('about_us_description') ?></p>
				</header>
				<div class="row justify-content-center">
					<div class="col-md-10">
						<div class="row ">
							<div class="col-md-4">
								<div class="overlay">
									<div class="overlay__item">
											<img src="<?php the_field('overlay_icon_1');  ?>">
											<h5 class="overlay__item--text"><?php the_field('about_us_overlay_text'); ?>	
											</h5>
									</div> 
									<img class="overlay__image" src="<?php the_field('about_us_image_1'); ?>">
								</div>
						</div>
						<div class="col-md-4">
							<div class="overlay overlay--wrapper">
								<div class="overlay__item">
									<img src="<?php the_field('overlay_icon_1');  ?>">
									<h5 class="overlay__item--text"><?php the_field('about_us_overlay_text'); ?>	
									</h5>
								</div>
								<img class="overlay__image" src="<?php the_field('about_us_image_2'); ?>">
							</div>
					</div>
					<div class="col-md-4">
						<div class="overlay overlay--wrapper">
							<div class="overlay__item">
								<img src="<?php the_field('overlay_icon_1');  ?>">
								<h5 class="overlay__item--text"><?php the_field('about_us_overlay_text'); ?>	
								</h5>
							</div>
							<img class="overlay__image" src="<?php the_field('about_us_image_1'); ?>">
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<!-- counter section -->
<div class="counter-section">
	<div class="container ">
		<div class="row justify-content-center" id="counter">
			<div class="col-md-2 col-sm-2  p-0">
				<div class="counter border-left">
					<h2 class="timer count-title count-number"><span class="counter-value" data-count="42">0</span></h2>
					<p class="count-text"><?php the_field('counter_canption_1'); ?></p>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 p-0">
				<div class="counter">
					<h2 class="timer count-title count-number" ><span class="counter-value" data-count="123">0</span></h2>
					<p class="count-text "><?php the_field('counter_canption_2'); ?></p>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 p-0">
				<div class="counter">
					<h2 class="timer count-title count-number"><span class="counter-value" data-count="15">0</span></h2>
					<p class="count-text "><?php the_field('counter_canption_3'); ?></p>
				</div></div>
				<div class="col-md-2 col-sm-2 p-0">
					<div class="counter">
						<h2 class="timer count-title count-number"><span class="counter-value" data-count="99">0</span></h2>
						<p class="count-text "><?php the_field('counter_canption_4'); ?></p>
					</div>
				</div>
				<div class="col-md-2 col-sm-2 p-0">
					<div class="counter">
						<h2 class="timer count-title count-number"><span class="counter-value" data-count="25">0</span></h2>
						<p class="count-text "><?php the_field('counter_canption_5'); ?></p>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- section for services -->
	<div class="services" id="services">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
						<header class="services__header">
						  	<h5 class="header__caption"><?php the_field('services_caption'); ?></h5>
							<h2 class="header__heading"><?php the_field('services_heading') ?></h2>
							<p class="header__description"><?php the_field('services_description') ?></p>
						</header>
				</div>
				<div class="row justify-content-center no-gutters">
					<div class="col-md-10">
						<div class="row no-gutters">
							<div class="col-md-4">
								<div class="services__items">
									<img src="<?php the_field('services_icon_1'); ?>">
									<div class="item">
										<p class="item__title"><?php the_field('services_field_1'); ?></p>
										<p><?php the_field('services_paragraph'); ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="services__items">
									<img src="<?php the_field('services_icon_2'); ?>">
									<div class="item">
										<p class="item__title"><?php the_field('services_field_2'); ?></p>
										<p><?php the_field('services_paragraph'); ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="services__items">
									<img src="<?php the_field('services_icon_3'); ?>">
									<div class="item">
										<p class="item__title"><?php the_field('services_field_3'); ?></p>
										<p><?php the_field('services_paragraph'); ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-12 p-5"> <hr/></div>
							<div class="col-md-4">
								<div class="services__items">
									<img src="<?php the_field('services_icon_4'); ?>">
									<div class="item">
										<p class="item__title"><?php the_field('services_field_4'); ?></p>
										<p><?php the_field('services_paragraph'); ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="services__items">
									<img src="<?php the_field('services_icon_5'); ?>">
									<div class="item">
										<p class="item__title"><?php the_field('services_field_5'); ?></p>
										<p><?php the_field('services_paragraph'); ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="services__items">
									<img src="<?php the_field('services_icon_6'); ?>">
									<div class="item">
										<p class="item__title"><?php the_field('services_field_6'); ?></p>
										<p><?php the_field('services_paragraph'); ?></p>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- section for unique design -->
	<div class="unique-section" style="background-image: url(<?php the_field('unique_image_background'); ?>)">
		<div class="unique-overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<header class="header">
						<h5 class="header__caption"><?php the_field('unique_caption'); ?></h5>
						<h2 class="header__heading"><?php the_field('unique_header'); ?></h2>
						<p class="header__description"></p>
					</header>
				</div>
			</div>
		</div>
		<div class="devices">
			<figure>
				<img src="<?php the_field('unique_image'); ?>">
			</figure>
		</div>
	</div>
	<!-- Service 2-->
	<div class="sevice2-section what-we-do">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<header class="what-we-do__header">
						<h5 class="header__caption"><?php the_field('accordion_caption'); ?></h5>
						<h2 class="header__heading"><?php the_field('accordion_heading'); ?></h2>
						<p class="header__description"><?php the_field('accordion_description'); ?></p>
					</header>
				</div>
			</div>
			<div class="row">
					<div class="col-md-6">
						<div class="image-viewer">
							<img id="large-img" src="<?php the_field('right_image'); ?>">
						</div>
					</div>
					<div class="col-md-6">
						<div id="services-right">
							<ul id="accordion">
								<li>	
									<h3 class="accordion" 
									data-srcs="<?php the_field('right_image'); ?>" data-id="1">
										<?php the_field('accordion_title_1'); ?>
									</h3>
									<div class="topic" data-id="1">	
										<?php the_field('accordion_content_1'); ?>
									</div>
								</li>
								<li>
									<h3 class="accordion" data-srcs="<?php the_field('slide-image_2'); ?>"  data-id="2">
										<?php the_field('accordion_title_2'); ?>
									</h3>
									<div class="topic" data-id="2">	
										<?php the_field('accordion_content_2'); ?>
									</div>	
								</li>
								<li>
									<h3 class="accordion" data-srcs="<?php the_field('slide-image_1'); ?>"  data-id="3">
										<?php the_field('accordion_title_3'); ?>
									</h3>
									<div class="topic" data-id="3">	
										<?php the_field('accordion_content_3'); ?>
									</div>	
								</li>
							</ul>

						</div>
					</div>
			</div>
		</div>
	</div>


	<!-- Quote 1 -->
	<div class="rslides_container testimonial_slider">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<ul class="rslides" id="slider1">
						<li>
							<div class="container">	
								<div class="row justify-content-center">	
									<div class="col-md-2">
										<img src="<?php the_field('qoute_image_1'); ?>">
									</div>
									<div class="col-md-8">
										<?php the_field('accordion_content_3'); ?>
										<div class="autor">
											<hr class="text-devider"/>
											<h5 class="head-caption"><?php the_field('quote_author_1'); ?></h5>
										</div>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="container">	
								<div class="row justify-content-center">	
									<div class="col-md-2">
										<img src="<?php the_field('qoute_image_1'); ?>">
									</div>
									<div class="col-md-8">
										<?php the_field('accordion_content_3'); ?>
										<div class="autor">
											<hr class="text-devider"/>
											<h5 class="head-caption"><?php the_field('quote_author_2'); ?></h5>
										</div>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="container">	
								<div class="row justify-content-center">	
									<div class="col-md-2">
										<img src="<?php the_field('qoute_image_1'); ?>">
									</div>
									<div class="col-md-8">
										<?php the_field('accordion_content_3'); ?>
										<div class="autor">
											<hr class="text-devider"/>
											<h5 class="head-caption"><?php the_field('quote_author_3'); ?></h5>
										</div>

									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Meet Our Team Section -->
	<div class="our-team">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<header class="our-team__header">
						<h5 class="header__caption"><?php the_field('team_caption'); ?></h5>
						<h2 class="header__heading"><?php the_field('team_heading'); ?></h2>
						<p class="header__description"><?php the_field('team_heading_copy') ?></p>
					</header>
					<div class="our-team__items mx-auto">
						<div class="row justify-content-center">
							<div class="col-md-3">
								<div class="overlay">
									<div class="overlay__item">
										<div class="overlay__item--hover">
											<a href="<?php the_field('team_social_icon_link_1'); ?>"><i class="fab fa-facebook-f"></i></a>
											<a href="<?php the_field('team_social_icon_link_1'); ?>"><i class="fab fa-twitter"></i></a>	
											<a href="<?php the_field('team_social_icon_link_1'); ?>"><i class="fab fa-instagram"></i></a>
											<a href="<?php the_field('team_social_icon_link_1'); ?>"><i class="fab fa-pinterest-p"></i></a>
										</div>		
									</div>
										<img class="overlay__item-image" src="<?php the_field('team_image_1'); ?>">
								</div>
							</div>
							<div class="col-md-3">
								<div class="overlay">
									<div class="overlay__item">
										<div class="overlay__item--hover">
											<a href="<?php the_field('team_social_icon_link_1'); ?>"><i class="fab fa-facebook-f"></i></a>
											<a href="<?php the_field('team_social_icon_link_1'); ?>"><i class="fab fa-twitter"></i></a>	
											<a href="<?php the_field('team_social_icon_link_1'); ?>"><i class="fab fa-instagram"></i></a>
											<a href="<?php the_field('team_social_icon_link_1'); ?>"><i class="fab fa-pinterest-p"></i></a>
										</div>		
									</div> 
									<img class="overlay__item-image" src="<?php the_field('team_image_2'); ?>">
								</div>
							</div>
							<div class="col-md-3">
								<div class="overlay">
									<div class="overlay__item">
										<div class="overlay__item--hover">
											<a href="<?php the_field('team_social_icon_link_1'); ?>"><i class="fab fa-facebook-f"></i></a>
											<a href="<?php the_field('team_social_icon_link_1'); ?>"><i class="fab fa-twitter"></i></a>	
											<a href="<?php the_field('team_social_icon_link_1'); ?>"><i class="fab fa-instagram"></i></a>
											<a href="<?php the_field('team_social_icon_link_1'); ?>"><i class="fab fa-pinterest-p"></i></a>
										</div>		
									</div> 
									<img class="overlay__item-image" src="<?php the_field('team_image_3'); ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Logos Section -->
	<div class="logo-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12">
					<div class="logo-list">
						<div class="row justify-content-center">
							<div class="col">
								<img src="<?php the_field('logo_1');	 ?>">
								<img src="<?php the_field('logo_2');	 ?>">
							</div>
							<div class="col">
								<img src="<?php the_field('logo_3');	 ?>">
								<img src="<?php the_field('logo_4');	 ?>">
							</div>
							<div class="col">
								<img src="<?php the_field('logo_5');	 ?>">
								<img src="<?php the_field('logo_6');	 ?>">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Works -->
	<div class="work-section" id="work">
		<div class="work-overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<header class="header">
						<h5 class="header__caption"><?php the_field('work_caption'); ?></h5>
						<h2 class="header__heading"><?php the_field('work_heading'); ?></h2>
						<p class="header__description"><?php the_field('work_description') ?></p>
					</header>
				</div>
			</div>
		</div>
	</div>

	<!-- 	work gallery -->
	<div id="work-gellery">
				<div class="grid-container">


					<?php 
         // the query
					$wpb_all_query = new WP_Query(array('post_type'=>'work','order' => 'ASC', 'post_status'=>'publish', 'posts_per_page'=>-1));
					?>

					<?php if ( $wpb_all_query->have_posts()) :  ?>

						<!-- the loop -->

						<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); 
							?> 
							<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

							<div id="box"  class="work-post single something" style="background-image: url('<?php //echo $thumb['0'];?>')"> 
								<figure><?php  the_post_thumbnail( 'full' ); ?> </figure> 
								<div class="overlay">		
									<a href="<?php the_permalink(); ?>">
										<i class="far fa-image" style="font-size: 24px;"></i>
										<h5 class="overlay__text"><?php the_title(); ?></h5>
										<?php the_excerpt(); ?>
									</a>
								</div>   
							</div>



						<?php endwhile; ?>

						<!-- end of the loop -->

						<?php wp_reset_postdata(); ?>

						<?php else : ?>


							<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
						<?php endif; ?>   

						<!-- <div class="loadmore-wrapper"><a href="#" id="loadMore">Load More</a> -->

							<!-- fuctioned with jquery -->

						</div>
			</div>

			<!-- Quote 2 -->
			<!-- Slideshow 3 -->

			<div class="rslides_container quote2_slider">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-10">
							<ul class="rslides" id="sliderquote2">
								<li>
									<div class="container">	
										<div class="row justify-content-center">
											<div class="col-md-2 col-sm-4">
														<img class="mx-auto d-block" src="<?php the_field('quote_2_image_1'); ?>">			
											</div>
											<div class="col-md-8">
												<?php the_field('accordion_content_3'); ?>
												<div class="autor">
													<hr class="text-devider"/>
													<h5 class="head-caption"><?php the_field('quote_author_1'); ?></h5>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="container">	
										<div class="row justify-content-center">	
											<div class="col-md-2">
												<img src="<?php the_field('qoute_image_1'); ?>">
											</div>
											<div class="col-md-8">
												<?php the_field('accordion_content_3'); ?>
												<div class="autor">
													<hr class="text-devider"/>
													<h5 class="head-caption"><?php the_field('quote_author_2'); ?></h5>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="container">	
										<div class="row justify-content-center">	
											<div class="col-md-2">
												<img src="<?php the_field('qoute_image_1'); ?>">
											</div>
											<div class="col-md-8">
												<?php the_field('accordion_content_3'); ?>
												<div class="autor">
													<hr class="text-devider"/>
													<h5 class="head-caption"><?php the_field('quote_author_3'); ?></h5>
												</div>

											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

<!-- section what people say -->
			<div class="testimonial" style="background-image: url(<?php the_field('testimonial__grid_background_image'); ?>)">
				<div class="testimonial__overlay"></div>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-10">
							<header class="testimonial__header">
								<h5 class="header__caption"><?php the_field('testimonial_grid_caption'); ?></h5>
								<h2 class="header__heading"><?php the_field('testimonial_grid_heading'); ?></h2>
								<p class="header__description"></p>
							</header>
							<div class="row">
								<div class="col-md-6 testimonial__items">
									<div class="row">
										<div class="col-md-3">
											<img src="<?php the_field('testimonial__grid_image_author_1'); ?>">
										</div>
										<div class="col-md-9">										
											<div class="item">	
												<h5 class="item__name"><?php the_field('quote_author_1'); ?></h5>
												<p class="item__header">Graphic Designer</p>
												<p class="item__testimony"><?php the_field('testimonial_grid_text'); ?></p>
											</div>	
										</div>
									</div>
								</div>
								<div class="col-md-6 testimonial__items">
									<div class="row">
										<div class="col-md-3">
											<img src="<?php the_field('testimonial__grid_image_author_2'); ?>">
										</div>
										<div class="col-md-9">										
											<div class="item">	
												<h5 class="item__name"><?php the_field('quote_author_1'); ?></h5>
												<p class="item__header">Graphic Designer</p>
												<p class="item__testimony"><?php the_field('testimonial_grid_text'); ?></p>
											</div>	
										</div>
									</div>
								</div>
								<div class="col-md-6 testimonial__items">
									<div class="row">
										<div class="col-md-3">
											<img src="<?php the_field('testimonial__grid_image_author_3'); ?>">
										</div>
										<div class="col-md-9">										
											<div class="item">	
												<h5 class="item__name"><?php the_field('quote_author_1'); ?></h5>
												<p class="item__header">Graphic Designer</p>
												<p class="item__testimony"><?php the_field('testimonial_grid_text'); ?></p>
											</div>	
										</div>
									</div>
								</div>
								<div class="col-md-6 testimonial__items">
									<div class="row">
										<div class="col-md-3">
											<img src="<?php the_field('testimonial__grid_image_author_4'); ?>">
										</div>
										<div class="col-md-9">										
											<div class="item">	
												<h5 class="item__name"><?php the_field('quote_author_1'); ?></h5>
												<p class="item__header">Graphic Designer</p>
												<p class="item__testimony"><?php the_field('testimonial_grid_text'); ?></p>
											</div>	
										</div>
									</div>
								</div>

							</div>
						</div>
					
					</div>
				</div>
			</div>


<!-- Blog -->
	<div class="blog" id="blog">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<header class="blog__header">
						<h5 class="header__caption"><?php the_field('blog_caption'); ?></h5>
						<h2 class="header__heading"><?php the_field('blog_heading'); ?></h2>
						<p class="header__description"></p>
					</header>
					<div class="row justify-content-center blog__items">
							
									<?php 
         // the query
					$wpb_all_query = new WP_Query(array('post_type'=>'post','order' => 'ASC', 'post_status'=>'publish', 'posts_per_page'=>-1));
					?>

					<?php if ( $wpb_all_query->have_posts()) :  ?>

						<!-- the loop -->

						<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); 
							?> 
							<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
						<div class="col-md-4">	
							<div   class="item" style="background-image: url('<?php //echo $thumb['0'];?>')"> 
								<figure class="item__header">
									<?php  the_post_thumbnail( 'full' ); ?> 
									<div class="date-label">
										<small>
											<h5><?php the_time('j') ?></h5>
											<p><?php the_time('M') ?></p>
										</small>
									</div> 
								</figure>
								<div class="item__info">		
									<a href="<?php the_permalink(); ?>">
										<h5 class="blog-title"><?php the_title(); ?></h5>
									</a>
										<?php the_excerpt(); ?>
											<hr/>
									<i class="fas fa-eye"></i>
									<?php if(function_exists('bac_PostViews')) { 
										    echo bac_PostViews(get_the_ID()); 
										}?>
									<i class="fas fa-comment"></i>
									<?php comments_number('0 Comments', '1' ); ?>
									
								</div>   
							</div>
						</div>



						<?php endwhile; ?>

						<!-- end of the loop -->

						<?php wp_reset_postdata(); ?>

						<?php else : ?>


							<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
						<?php endif; ?>   

						<!-- <div class="loadmore-wrapper"><a href="#" id="loadMore">Load More</a> -->

							<!-- fuctioned with jquery -->

						
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- section Map -->
			<div class="map-section" style="background-image: url(<?php the_field('testimonial__grid_background_image'); ?>)">
				<div class="map-overlay"></div>
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-md-12">
							<header id="button">
								<i class="fas fa-map-marker-alt"></i>
								<h5 class="map__button">Open Map</h5>
							</header>
								<iframe id="effect" class="newClass" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d251624.4374480791!2d125.44389848262517!3d9.797185981333763!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x330136c91863193d%3A0x8d809adba4083ee4!2sSurigao+City%2C+Surigao+del+Norte!5e0!3m2!1sen!2sph!4v1553072271061" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

						</div>
					</div>
				</div>
			</div>



</main>




		<?php get_footer();


